const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');

const Comment = mongoose.model('comments');

module.exports = app => {
  app.get('/api/comments', requireLogin, async (req, res) => {
    const comments = await Comment.find({ _user: req.user.id });
    res.send(comments);
  });

  app.get('/api/comments/:blogId', async (req, res) => {
    const comments = await Comment.find({ _blog: req.params.blogId });
    res.send(comments);
  });

  app.post('/api/comments/:blogId', requireLogin, async (req, res) => {
    const { body, parentId } = req.body;
    var ar = [];
    ar.push(req.user.id);

    const comment = new Comment({
      author: req.user.name,
      body,
      _user: req.user.id,
      _blog: req.params.blogId,
      _comment: parentId,
      response: ar,
      dateSent: Date.now()
    });

    try {
      const newComment = await comment.save();
      res.send(newComment);
    } catch (err) {
      res.status(422).send(err);
    }
  });

  app.post(
    '/api/comments/:commentId/:choice',
    requireLogin,
    async (req, res) => {
      await Comment.updateOne(
        {
          _id: req.params.commentId,
          response: {
            $nin: [req.user.id]
          }
        },
        {
          $inc: { [req.params.choice]: 1 },
          $push: {
            response: req.user.id
          }
        }
      ).exec();
      res.send(req.user);
    }
  );
};
