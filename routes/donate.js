const mongoose = require('mongoose');

const Donor = mongoose.model('donors');

module.exports = app => {
  app.get('/donation', (req, res) => {
    res.render('donation', { title: 'Donate Us', auth: req.user });
  });

  app.post('/api/donate', async (req, res) => {
    const {
      firstName,
      lastName,
      date,
      month,
      year,
      pan,
      email,
      phone,
      address,
      pin,
      city,
      state,
      country,
      notes
    } = req.body;
    const donor = new Donor({
      firstName,
      lastName,
      dateOfBirth: new Date(year, month, date),
      pan,
      email,
      phone,
      address,
      pin,
      city,
      state,
      country,
      notes,
      donateAt: Date.now()
    });

    await donor.save();
    res.redirect('/donation');
  });
};
