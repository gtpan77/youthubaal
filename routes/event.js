const mongoose = require('mongoose');
const moment = require('moment');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
});

const requireLogin = require('../middlewares/requireLogin');
const removeScript = require('../services/removeScript');

const Event = mongoose.model('events');
const threshhold = 3;

module.exports = app => {
  app.get('/events/new', requireLogin, (req, res) => {
    res.render('eventForm', { title: 'Create Event', auth: req.user });
  });

  app.get('/events', async (req, res) => {
    const events = await Event.find({})
      .limit(threshhold)
      .sort({ dateSent: -1 });
    res.render('events', {
      title: 'Events - YouthUbaal',
      auth: req.user,
      events: events,
      moment: moment
    });
  });

  app.get('/events/page/:pageNo', async (req, res) => {
    const events = await Event.find({})
      .limit(threshhold)
      .skip(threshhold * (Number(req.params.pageNo) - 1))
      .sort({ dateSent: -1 });
    res.render('events', {
      title: 'events - YouthUbaal',
      auth: req.user,
      events: events,
      moment: moment
    });
  });

  app.get('/event/:eventId', async (req, res) => {
    const event = await Event.findOne({
      _id: req.params.eventId
    });
    res.render('event', {
      title: event.title,
      auth: req.user,
      event: event,
      moment: moment
    });
  });

  app.get('/api/events/all', async (req, res) => {
    const events = await Event.find({});
    res.send(events);
  });

  app.get('/api/events', requireLogin, async (req, res) => {
    const events = await Event.find({ _user: req.user.id });
    res.send(events);
  });

  app.get('/api/events/size', async (req, res) => {
    const size = await Event.count();
    res.send({ length: ((size + threshhold - 1) / threshhold) >> 0 });
  });

  app.get('/api/events/:limit', async (req, res) => {
    const size = await Event.count();
    const events = await Event.find({})
      .limit(threshhold)
      .skip(threshhold * req.params.limit);
    res.send(events);
  });

  app.post(
    '/api/events',
    requireLogin,
    upload.single('photo'),
    async (req, res) => {
      const { name, url, body } = req.body;

      const event = new Event({
        name: removeScript(name),
        url,
        body: removeScript(body),
        photo: req.file.path,
        dateSent: Date.now()
      });

      try {
        await event.save();
        res.redirect('/event/' + event._id);
      } catch (err) {
        res.status(422).send(err);
      }
    }
  );

  app.get('/api/event/:eventId', async (req, res) => {
    const event = await Event.findOne({
      _id: req.params.eventId
    });
    res.send(event);
  });
};
