const mongoose = require('mongoose');
const moment = require('moment');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
});

const requireLogin = require('../middlewares/requireLogin');
const removeScript = require('../services/removeScript');

const Blog = mongoose.model('blogs');
const threshhold = 3;

module.exports = app => {
  app.get('/blogs/new', requireLogin, (req, res) => {
    res.render('blogForm', { title: 'Blog Entry', auth: req.user });
  });

  app.get('/blogs', async (req, res) => {
    const blogs = await Blog.find({})
      .limit(threshhold)
      .sort({ dateSent: -1 });
    res.render('blogs', {
      title: 'Blogs - YouthUbaal',
      auth: req.user,
      blogs: blogs,
      moment: moment
    });
  });

  app.get('/blogs/page/:pageNo', async (req, res) => {
    const blogs = await Blog.find({})
      .limit(threshhold)
      .skip(threshhold * (Number(req.params.pageNo) - 1))
      .sort({ dateSent: -1 });
    res.render('blogs', {
      title: 'blogs - YouthUbaal',
      auth: req.user,
      blogs: blogs,
      moment: moment
    });
  });

  app.get('/blog/:blogId', async (req, res) => {
    const blog = await Blog.findOne({
      _id: req.params.blogId
    });
    res.render('blog', {
      title: blog.title,
      auth: req.user,
      blog: blog,
      moment: moment
    });
  });

  app.get('/api/blogs/all', async (req, res) => {
    const blogs = await Blog.find({});
    res.send(blogs);
  });

  app.get('/api/blogs', requireLogin, async (req, res) => {
    const blogs = await Blog.find({ _user: req.user.id });
    res.send(blogs);
  });

  app.get('/api/blogs/size', async (req, res) => {
    const size = await Blog.count();
    res.send({ length: ((size + threshhold - 1) / threshhold) >> 0 });
  });

  app.get('/api/blogs/:limit', async (req, res) => {
    const size = await Blog.count();
    const blogs = await Blog.find({})
      .limit(threshhold)
      .skip(threshhold * req.params.limit);
    res.send(blogs);
  });

  app.post(
    '/api/blogs',
    requireLogin,
    upload.single('photo'),
    async (req, res) => {
      const { title, author, url, body } = req.body;
      var ar = [];
      ar.push(req.user.id);

      const blog = new Blog({
        title,
        author,
        url,
        body: removeScript(body),
        photo: req.file.path,
        _user: req.user.id,
        response: ar,
        dateSent: Date.now()
      });

      try {
        await blog.save();
        res.redirect('/blog/' + blog._id);
      } catch (err) {
        res.status(422).send(err);
      }
    }
  );

  app.get('/api/blog/:blogId', async (req, res) => {
    const blog = await Blog.findOne({
      _id: req.params.blogId
    });
    res.send(blog);
  });

  app.post('/api/blog/:blogId/:choice', requireLogin, async (req, res) => {
    await Blog.updateOne(
      {
        _id: req.params.blogId,
        response: {
          $nin: [req.user.id]
        }
      },
      {
        $inc: { [req.params.choice]: 1 },
        $push: {
          response: req.user.id
        }
      }
    ).exec();
    res.send(req.user);
  });
};
