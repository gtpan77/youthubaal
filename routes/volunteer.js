const mongoose  = require('mongoose');

const Volunteer = mongoose.model('volunteers');

module.exports = app => {
  app.get('/volunteer-with-yu', (req, res) => {
    res.render('volunteer', { title: 'Volunteer With YouthUbaal', auth: req.user });
  });

  app.get('/api/volunteer', async (req, res) => {
    const volunteers = await Volunteer.find({});
    res.send(volunteers);
  });

  app.post('/api/volunteer', async (req, res) => {
    console.log(req.body);
    const { firstName, lastName, email, phone, city, state, country } = req.body;

    const volunteer = new Volunteer({
      firstName,
      lastName,
      email,
      phone,
      city,
      state,
      country,
      applied: Date.now()
    });

    await volunteer.save();
    res.redirect('/volunteer-with-yu');
  });
};
