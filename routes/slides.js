const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');

const Slide = mongoose.model('slides');

module.exports = app => {
  app.get('/api/slides', async (req, res) => {
    const slides = await Slide.find({});
    res.send(slides);
  });

  app.post('/api/slides', requireLogin, async (req, res) => {
    const { caption, slogan, image } = req.body;

    const slide = new Slide({
      caption,
      slogan,
      image
    });

    try {
      await slide.save();
      res.send(slide);
    } catch (err) {
      res.status(422).send(err);
    }
  });
};
