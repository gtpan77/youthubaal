$(document).ready(function() {
  function removeScript(html) {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(html)) {
      html = html.replace(SCRIPT_REGEX, '');
    }
    return html;
  }

  $("input[name='title']").on('input', function() {
    const htmlString = $(this).val();
    $('.blog_review .blog_title').html(removeScript(htmlString));
  });

  $("textarea[name='body']").on('input', function() {
    const htmlString = $(this).val();
    $('.blog_review .blog_body').html(removeScript(htmlString));
  });
});
