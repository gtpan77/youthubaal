var pathname = window.location.pathname.split('/');
var filename = pathname[pathname.length - 1];

function blogTemplate(blog) {
  var html =
    `<div class="card">
    <h3 class="card-header">
    <a href="/blog/` +
    blog._id +
    `">` +
    removeScript(blog.title) +
    `</a>
    </h3>
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <footer class="blockquote-footer">Written By <cite title="Source Title">` +
    blog.author +
    `</cite></footer>
        <p>` +
    removeScript(blog.body) +
    `</p>
      </blockquote>
    </div>
    <div class="card-header">

    </div>
  </div>`;
  return html;
}

function commentTemplate(comment, margin) {
  var html =
    `<div style="margin-left: ` +
    margin +
    `%;" class="card" id="comment-` +
    comment._id +
    `">
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <footer class="blockquote-footer"><cite title="Source Title">` +
    comment.author +
    `</cite></footer>
        <p>` +
    removeScript(comment.body) +
    `</p>
      </blockquote>
    </div>
    <div class="card-header comment-footer">
      <button class="commentReplyButton">Reply</button>
      <div class="toggleReplyForm" style="display: none">
        <textarea class="form-control commentReplyForm" name="body" rows="3"></textarea>
        <button class="btn btn-primary commentReplySubmit">Submit</button>
      </div>
    </div>
  </div>`;
  return html;
}

function removeScript(html) {
  var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
  while (SCRIPT_REGEX.test(html)) {
    html = html.replace(SCRIPT_REGEX, '');
  }
  return html;
}

// $(document).ready(function() {
//   $.notify('Login Please', { className: 'info', position: 'bottom right' });
// });
