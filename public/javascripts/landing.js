$(document).ready(function() {
  $.ajax({
    type: 'GET',
    url: '/api/slides',
    success: function(val) {
      const children = $('.carousel-inner .carousel-item').children();
      children.each(function(i) {
        $(this).attr('src', val[i].image);
      });
    }
  });
});
