$(document).ready(function() {
  function removeScript(html) {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(html)) {
      html = html.replace(SCRIPT_REGEX, '');
    }
    return html;
  }

  $("input[name='name']").on('input', function() {
    const htmlString = $(this).val();
    $('.event_review .event_name').html(removeScript(htmlString));
  });

  $("textarea[name='body']").on('input', function() {
    const htmlString = $(this).val();
    $('.event_review .event_body').html(removeScript(htmlString));
  });

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
});
