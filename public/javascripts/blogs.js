$(document).ready(function() {
  var pathname = window.location.pathname.split('/');
  var filename =
    pathname.length === 4 ? Number(pathname[pathname.length - 1]) : 1;

  function removeScript(html) {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(html)) {
      html = html.replace(SCRIPT_REGEX, '');
    }
    return html;
  }

  $.ajax({
    type: 'GET',
    url: '/api/blogs/size',
    success: function(count) {
      if (filename > count.length) {
        history.back();
        // $.notify("No such page is available", {
        //   className: "info",
        //   globalPosition: "bottom right"
        // });
        return;
      }

      if (filename === 1) {
        $('#newer').addClass('disabled');
      } else {
        $('#newer').removeClass('disabled');
        if (filename === 2) {
          $('#newer a').attr('href', '/blogs');
        } else {
          $('#newer a').attr('href', '/blogs/page/' + (filename - 1));
        }
      }

      if (filename === count.length) {
        $('#older').addClass('disabled');
      } else {
        $('#older a').removeClass('disabled');
        $('#older a').attr('href', '/blogs/page/' + (filename + 1));
      }
    }
  });
});
