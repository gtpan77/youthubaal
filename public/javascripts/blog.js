$(document).ready(function() {
  var pathname = window.location.pathname.split("/");
  var filename = pathname[pathname.length - 1];

  /*
  function recurse(comments, marked, current, depth) {
    const el = $('.comments');
    const html = commentTemplate(comments[current], depth * 3);
    el.append(html);
    marked[current] = 1;
    comments.forEach(function(comment, idx) {
      if (marked[idx] === 1) return;
      if (comments[current]._id === comment._comment) {
        recurse(comments, marked, idx, depth + 1);
      }
    });
  }
*/

  /*
  $.ajax({
    type: 'GET',
    url: '/api/blog/' + filename,
    success: function(blog) {
      const el = $('.blog-container');
      const html = blogTemplate(blog);
      el.append(html);
    }
  });
*/

  /*
  $.ajax({
    type: 'GET',
    url: '/api/comments/' + filename,
    success: function(comments) {
      var marked = [];
      console.log(comments);
      for (let i = 0; i < comments.length; i++) marked.push(0);
      comments.forEach(function(comment, idx) {
        if (marked[idx] === 1) return;
        recurse(comments, marked, idx, 0);
      });
    }
  });

  $('#commentSubmit').on('click', function() {
    $.ajax({
      type: 'POST',
      url: '/api/comments/' + filename,
      data: { body: $('#commentForm').val(), parentId: -1 },
      dataType: 'json',
      success: function(res) {
        console.log(res);
        const el = $('.comments');
        const html = commentTemplate(res);
        el.append(html);
      }
    });
  });

  $('#comments-container').on('click', '.commentReplyButton', function() {
    var x = $(this)
      .parent()
      .children('.toggleReplyForm');
    x.toggle();
  });

  $('#comments-container').on('click', '.commentReplySubmit', function() {
    const id = Number(
      $(this)
        .parent()
        .parent()
        .parent()
        .attr('id')
        .split('-')[1]
    );
    console.log(id);
    $.ajax({
      type: 'POST',
      url: '/api/comments/' + filename,
      data: {
        body: $(this)
          .parent()
          .children('.commentReplyForm')
          .val(),
        parentId: id
      },
      dataType: 'json',
      success: function(res) {
        console.log(res);
        const el = $('.comments');
        const html = commentTemplate(res);
        el.append(html);
      }
    });
  });
  */
});
