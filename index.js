const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const request = require('request');
const keys = require('./config/keys');

mongoose.connect(keys.mongoURI);
autoIncrement.initialize(mongoose.connection);

require('./models/Slide');
require('./models/User');
require('./models/Blog');
require('./models/Comment');
require('./models/Event');
require('./models/Volunteer');
require('./models/Donor');
require('./services/passport');

const slides = require('./routes/slides');
const auth = require('./routes/auth');
const donate = require('./routes/donate');
const blog = require('./routes/blog');
const comment = require('./routes/comment');
const event = require('./routes/event');
const volunteer = require('./routes/volunteer');

const requireLogin = require('./middlewares/requireLogin');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static('uploads'));

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
  res.render('index', { title: 'website', auth: req.user });
});

app.get('/user/:userId', (req, res) => {
  res.render('user', { title: req.params.userId, auth: req.user });
});

slides(app);
auth(app);
donate(app);
blog(app);
comment(app);
event(app);
volunteer(app);

app.get('*', (req, res) => {
  res.send('<h1>The requested URL was not found on this server.</h1>');
});

const PORT = process.env.PORT || 8000;
app.listen(PORT);
