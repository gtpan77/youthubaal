const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const blogSchema = new Schema({
  title: String,
  author: String,
  url: String,
  photo: String,
  body: String,
  upvote: { type: Number, default: 0 },
  downvote: { type: Number, default: 0 },
  _user: { type: Number, ref: 'User' },
  response: [Number],
  dateSent: Date,
  lastResponded: Date
});

blogSchema.plugin(autoIncrement.plugin, 'blogs');
mongoose.model('blogs', blogSchema);
