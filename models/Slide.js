const mongoose = require('mongoose');
const { Schema } = mongoose;

const slideSchema = new Schema({
  caption: String,
  slogan: String,
  image: String
});

mongoose.model('slides', slideSchema);
