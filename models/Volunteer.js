const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const volunteerSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  phone: String,
  city: String,
  state: String,
  country: String,
  applied: Date
});

volunteerSchema.plugin(autoIncrement.plugin, 'volunteers');
mongoose.model('volunteers', volunteerSchema);
