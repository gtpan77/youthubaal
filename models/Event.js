const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const eventSchema = new Schema({
  name: String,
  eventDate: Date,
  url: String,
  photo: String,
  body: String,
  dateSent: Date
});

eventSchema.plugin(autoIncrement.plugin, 'events');
mongoose.model('events', eventSchema);
