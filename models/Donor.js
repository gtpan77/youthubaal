const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const donorSchema = new Schema({
  firstName: String,
  lastName: String,
  dateOfBirth: Date,
  panNumber: String,
  email: String,
  phone: String,
  address: String,
  city: String,
  pin: String,
  state: String,
  country: String,
  notes: String,
  donateAt: Date
});

donorSchema.plugin(autoIncrement.plugin, 'donors');
mongoose.model('donors', donorSchema);
