const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const commentSchema = new Schema({
  author: String,
  body: String,
  upvote: { type: Number, default: 0 },
  downvote: { type: Number, default: 0 },
  _user: { type: Number, ref: 'User' },
  _blog: { type: Number, ref: 'Blog' },
  _comment: { type: Number, ref: 'Comment' },
  response: [Number],
  dateSent: Date
});

commentSchema.plugin(autoIncrement.plugin, 'comments');
mongoose.model('comments', commentSchema);
