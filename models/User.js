const mongoose = require('mongoose');
const { Schema } = mongoose;
const autoIncrement = require('mongoose-auto-increment');

const userSchema = new Schema({
  googleId: String,
  email: String,
  userId: String,
  name: String,
  phone: String,
  photo: String,
  reputation: { type: Number, default: 0 }
});

userSchema.plugin(autoIncrement.plugin, 'users');
mongoose.model('users', userSchema);
